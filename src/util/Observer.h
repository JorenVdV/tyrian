/**
 * @file Observer.h
 * @class util::Observer
 *
 * @brief base class for all observers
 *
 * @author Joren Van de Vondel
 *
 *  Created on: Dec 23, 2013
 */

#ifndef OBSERVER_H_
#define OBSERVER_H_

#include "../controller/ObservableGame.h"


namespace util {

class Observer {
public:
	virtual ~Observer();

	virtual void update();
};

}

#endif /* OBSERVER_H_ */
