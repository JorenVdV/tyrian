/*
 * Observer.cpp
 *
 *  Created on: Dec 23, 2013
 *      Author: jweiren
 */

#include "Observer.h"


namespace util {

/**
 * @brief Destructor for class Obsorver
 *
 */
Observer::~Observer() {
}
/**
 * @brief abstract function
 * @param game
 * 			game of wich state is to be checked
 */
void Observer::update(){

}

} /* namespace engine */
