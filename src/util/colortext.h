/*
 * @file colortext.h
 * @brief used to color output text in the game
 * @author Joren Van de Vondel
 *  Created on: Jan 6, 2015
 */

#ifndef SRC_UTIL_COLORTEXT_H_
#define SRC_UTIL_COLORTEXT_H_

#include <stdio.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"



#endif /* SRC_UTIL_COLORTEXT_H_ */
