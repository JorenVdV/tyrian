/*
 * @file id_generator.h
 * @brief contains the fucntion for the id generation of objects
 * @author Jore Van de Vondel
 *
 *  Created on: 24 Dec 2014
 */

#ifndef ID_GENERATOR_H_
#define ID_GENERATOR_H_

#include <cmath>
#include <stdio.h>

unsigned int id_generator(){
	static unsigned int id=0;
	return id++;
}




#endif /* ID_GENERATOR_H_ */
