/*
 * utilf.cpp
 *
 *  Created on: Jan 9, 2015
 *      Author: jweiren
 */

#include "utilf.h"
#include <stdio.h>

double get_e_distance(std::pair<double, double> a, std::pair<double, double>b){
//	printf("dist((%f, %f), (%f, %f)) = %f\n",
//			a.first, a.second, b.first, b.second,
//			sqrt(pow(a.first+b.first, 2)+pow(a.second+b.second, 2)));
	return sqrt(pow(a.first-b.first, 2)+pow(a.second-b.second, 2));
}

void psdes(unsigned long &lword, unsigned long &irword)
{
	const int NITER=4;
	static const unsigned long c1[NITER]={
		0xbaa96887L, 0x1e17d32cL, 0x03bcdc3cL, 0x0f33d1b2L};
	static const unsigned long c2[NITER]={
		0x4b0f3b58L, 0xe874f0c3L, 0x6955c5a6L, 0x55a7ca46L};
	unsigned long i,ia,ib,iswap,itmph=0,itmpl=0;

	for (i=0;i<NITER;i++) {
		ia=(iswap=irword) ^ c1[i];
		itmpl = ia & 0xffff;
		itmph = ia >> 16;
		ib=itmpl*itmpl+ ~(itmph*itmph);
		irword=lword ^ (((ia = (ib >> 16) |
			((ib & 0xffff) << 16)) ^ c2[i])+itmpl*itmph);
		lword=iswap;
	}
}


double ran4()
{
	static int idum =10;
#if defined(vax) || defined(_vax_) || defined(__vax__) || defined(VAX)
	static const unsigned long jflone = 0x00004080;
	static const unsigned long jflmsk = 0xffff007f;
#else
	static const unsigned long jflone = 0x3f800000;
	static const unsigned long jflmsk = 0x007fffff;
#endif
	unsigned long irword,itemp,lword;
	static int idums = 0;

	if (idum < 0) {
		idums = -idum;
		idum=1;
	}
	irword=idum;
	lword=idums;
	psdes(lword,irword);
	itemp=jflone | (jflmsk & irword);
	++idum;
	return (*(float *)&itemp)-1.0;
}


