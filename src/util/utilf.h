/*
 * @file utilf.h
 * @brief contains utility functions for the game
 *
 * @author Joren Van de Vondel
 *
 *  Created on: Jan 8, 2015
 */

#ifndef UTILF_H
#define UTILF_H

#include <cmath>
#include <utility>

//artithmetic function

//! get euclidian distance between 2 points
/*!
  Computes the euclidian distance between 2 given points, given in pair format
  \param point 1
  \param point 2
 */
double get_e_distance(std::pair<double, double> a, std::pair<double, double>b);

//! helper fucntion for ran4
/*!
  Function pulled from the numerical recipes library
 */
void psdes(unsigned long &lword, unsigned long &irword);

//! generate random numbers
/*!
 * Used for the chance an enemy gets for shooting
 * Fucntion comes from the numerical recipes library
 */
double ran4();
#endif



