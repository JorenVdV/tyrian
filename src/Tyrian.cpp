//============================================================================
// Name        : Tyrian.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <unistd.h>

#include "model/Object.h"
#include "view/GameView.h"
#include "controller/Game.h"
#include "util/colortext.h"
#include "util/utilf.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

int main() {
	ty::Game* game = new ty::Game();
	view::GameView* view = new view::GameView(game, true);

	game->start();

	delete game;
	delete view;


	return 0;
}
