/*
 * @file Game.h
 * @class ty::Game
 *
 * @author Joren Van de Vondel
 *  Created on: 24 Dec 2014
 */

#ifndef GAME_H_
#define GAME_H_

#include "../model/ABulletFactory.h"
#include "../model/ACannonFactory.h"
#include "../model/ALevelFactory.h"
#include "../model/AShipFactory.h"
#include "../model/ATileFactory.h"

#include "ObservableGame.h"

#include <map>
#include <set>
#include <SFML/System.hpp>


namespace ty {

//! GameState enum
/*
 * enumeration containing all possible states that a game can be in
 */
enum GameState{STARTUP, MAIN, GAME, PAUSED, DEAD, WON};
//! Movement enum
/*
 * enumeration conaining all possible movements that can be made by
 * the player, serves as an translation for the sfml equivalent in
 * the gameview.
 */
enum Movement{UP, DOWN, LEFT, RIGHT};

//! Game class
/*
 * Main class used by the game. This class represents the controller
 * and thus provides a link between the model and the view. It is also
 * responsible for updating the model.
 */
class Game: public  ObservableGame{

public:
	//! constructor
	/*!
	  The constructor provides all the necessary implementation of the model.
	  It will make all the necessary objects and link them.
	 */
	Game();

	//! destructor
	/*!
	  The destructor makes sure that the are no untracked objects after
	  termination of the program. It will stop the factories and delete
	  them afterwards.
	 */
	virtual ~Game();

	// interface for setting up the game

	//! start, used to start the game from the main thread
	/*!
	   This function sets all the necessary members for making the game run,
	   those that can not be handled inside the constructor.
	   It will parse the levels provided in the level.txt file and the
	   tiles provided in the tiles.txt file.
	   It will also continuously call upon the function loop until the
	   function stop has been called upon.
	 */
	void start();

	//! stop, used to stop the game completely.
	/*!
	  main purpose of this function is to stop the factories and set the
	  data member _ended to true, this will stop the loop in start.
	 */
	void stop();

	// interface for visualization

	//! get_fps, used to get the frame rate of the game
	/*!
	  function is for development/performance testing purposes.
	  \return the frame rate / second
	 */
	double get_fps();

	//! get_score, used to return the current score of the player
	/*!
	  used by the view for displaying the players score on the screen.
	  \return the score
	 */
	unsigned int get_score();

	//! get_center, returns the current y-value of the middle of the map
	/*!
	  The center of the game is determined by a single double that
	  indicated the y-value of the center of the screen. This is the
	  only variable needed since it is a vertical scroller
	  \return the y-value of the center
	 */
	double get_center();

	//! get_player, used for getting the data model of the player
	/*!
	  This is used by the view to access the player's attributes, the view
	  should not alter the data model, only read from it.
	  \return pointer to the player object
	 */
	Ship* get_player();

	//! get_enemies, returns a map containing all enemies mapped by their object id
	/*!
		This is used by the view to access the enemy ships data, should only read
		from this data not alter it.
		\return a map containing all enemy object mapped by their id
	 */
	std::map<unsigned int, Ship*> get_enemies();

	//! get_bullets, returns a map containing all bullets mapped by their object id
	/*!
	  This is used by the view to access the bullets data, should only read from
	  these objects, not alter them.
	  \return a map containing all bullets mapped by their id
	 */
	std::map<unsigned int, Bullet*> get_bullets();

	//! get_tiles, returns a map containing all tiles mapped by their object id
	/*!
	  This is used by the view to access the tiles data, should only read data from
	  these objects , not alter them.
	  \return a map containing all tiles mapped by their id.
	 */
	std::map<unsigned int, Tile*> get_tiles();

	//! get_ctiles, return the current tiles used by the game
	/*!
	  During the game there should always be 3 tiles loaded in the system.
	  This function gives acces to those 3 tiles, should only be used to
	  display them, not to alter them.
	  \return vector containing the 3 tiles
	 */
	std::vector<Tile*> get_ctiles();

	//! get_levels, returns all the levels parsed by the game.
	/*!
	  This function should be used as an interface for the view to display
	  all the names of the levels. Not to alter their content.
	  \return map containing all levels mapped by their id
	 */
	std::map<unsigned int, Level*> get_levels();

	//! get_clevel, returns the current level, if one is set
	/*!
	  \return the level that is currently set to play
	 */
	Level* get_clevel();

	//!get_explosion, gives all the locations on which an explosion took place
	/*!
	  currently not used, can be used to display explosions and play a sound.
	  \return set with all coordinates of explosions
	 */
	std::set<std::pair<double, double >> get_explosions();

	// interface for input

	//! get game state
	/*!
	  \return the current state of the game
	 */
	GameState get_game_state();

	//! enter main, makes the game enter the main game mode
	/*!
	 This function set the gamestate to main, referring to the level
	 selection menu
	 */
	void enter_main();

	//! start game level-id
	/*!
	  starts up a level defined by the level id
	  \param id the level id
	 */
	void start_game(unsigned int id); 		// MAIN -> GAME

	//! quit game
	/*!
	 allows the the input to end the game and return to the level selection
	 menu while playing a level or being paused on a level.
	 */
	void quit_game(); 		//GAME/PAUSED -> MAIN

	//! pause game
	/*!
	 allows the input to pause a game while playing a game
	 */
	void pause_game(); 		// GAME -> PAUSED

	//! resume game
	/*!
	  allows the input to resume the game from the paused screen
	 */
	void resume_game(); 	// PAUSED -> GAME

	//! exit dead/won screen
	/*!
	  allows the input to exit the dead/won screen and return to
	  the main level selection menu.
	 */
	void exit_dead_won();	//  DEAD/WON -> MAIN

	//! move player direction
	/*!
	  Moves the player in a direction
	  \param the directon the player is to move
	 */
	void player_move(Movement mov);

	//! player shoot
	/*!
	  makes the player shoot a bullet.
	 */
	void player_shoot();

private:
	//private functions

	//! main loop of the game
	/*!
	  This will be iterative called upon from the start function.
	  In this fucntion, each gamestate relative tasks will be performend
	  (if any), and the observers will be notified.
	 */
	void loop(); // main loop of game, will be called upon by start

	//! player dies
	/*!
	  The player has died while in game state, sets the gamestate to dead
	  and ends the level.
	 */
	void player_dies(); // player dies before end of level

	//! player wins
	/*!
	  the player has ended the level without dying, sets the gamestate to
	  won and will display the score.
	 */
	void player_wins();	// player wins the level, return to main menu

	//! stop level
	/*!
	  ends a level by stopping all the factories and reverting the
	  current level to none.
	 */
	void stop_level();

	//! start level
	/*!
	  loads up a level from the _current_level member
	  if none is set, this function will do nothing.
	 */
	void start_level();


	//! collision check
	/*!
	 checks if there are any collisions by objects and will take actions
	 if a collision if found;
	 bullets will be destroyed,
	 ships will take damages
	 */
	void update_collisions();

	//! check which enemies have died
	/*!
	  Verifies the damage of each enemy, if damage has lowered to 0
	  the enemy is dead and should be deleted.
	 */
	void update_enemy_death();

	//! check if player is still alive
	/*!
	  Verifies the damage of the player, if the player is dead, then
	  the level should be stopped and player_dies is called upon
	 */
	void update_player_death();

	//! update player's position
	/*!
	  Takes care of the auto scroll movement of the player,
	  BUG: if the player falls of the screen he disappears?
	 */
	void update_player_position();

	//! update enemy position
	/*!
	  Updates the position of all the enemies in the game.
	  if an enemy is to far down from the field center it
	  will be deleted.

	  At the moment there are no real movements made by this
	  function, can however be changed by adding moving enemies.
	 */
	void update_enemy_position();

	//! update bullet position
	/*!
	  updates the position of all the bullets in the game, based on
	  their direction and the level bullet speed.
	  If there are any bullets outside the field they are discarded.
	 */
	void update_bullet_position();

	//! update tiles
	/*!
	 updates the tiles currently loaded by the game,
	 if a new tile is necessary, it will be copied from the tiles list and
	 its position will be set accordingly. The tiles are copies since a single
	 tile specification can be used multiple times at the same moment.
	 */
	void update_tiles();

	//! update enemy shootings
	/*!
	  Updates for each enemy the chance to shoot, this is done completely random.
	  The random function used by this function comes from the numerical recipes
	  libraries to ensure complete randomness in the shooting. Threshold is set
	  statical could be set in the level in extension maybe.

	  ran4 delivers a random double in the interval [0,1]
	 */
	void update_enemies_shoot();

	//! spawn enemies on a given tile
	/*!
	 providing a tile, enemies are spawned using the list of coordinates that
	 can be set in the tiles specifications.
	 \param tile of which we need to spawn the enemies.
	 */
	void spawn_enemies(Tile* t);

	//! parse levels and tiles
	/*!
	  read all the files from the level.txt and tiles.txt files and makes
	  new levels and tiles from these specifications.
	 */
	void parse_levels();

	// local settings
	GameState _gamestate;
	bool _ended;
	double _field_center; //vertical scroller horizontal doesn't matter
	unsigned int _score;

	sf::Clock _clock_game_start;
	sf::Clock _clock_last_update;
	sf::Clock _clock_level_start;
	sf::Clock _clock_last_shot;
	sf::Clock _clock_calc;

	double _last_update_time;

	//Factories
	ABulletFactory* _bfactory;
	ACannonFactory* _cfactory;
	ALevelFactory* _lfactory;
	AShipFactory* _sfactory;
	ATileFactory* _tfactory;
	// models
	Ship* _player;
	Level* _current_level; //level that is currently played,
	// only accessible when in GAME/LEVEL mode
	std::vector<Tile*> _current_tiles; // contains all the tiles
	// in order of appearance.
	int _next_tile_index;

	std::map<unsigned int, Ship*> _enemies;
	std::map<unsigned int, Bullet*> _bullets;
	std::map<unsigned int, Level*> _levels;
	std::map<unsigned int, Tile*> _tiles;
	std::set<std::pair<double, double>> _explosions;

};



} /* namespace ty */

#endif /* GAME_H_ */
