/*
 * Game.cpp
 *
 *  Created on: Jan 9, 2015
 *      Author: jweiren
 */

#include "Game.h"
#include "../util/colortext.h"
#include "../util/utilf.h"

#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>

namespace ty {

Game::Game():
	_gamestate(STARTUP),
	_ended(false)
{
	//init all clocks

	_last_update_time=0;

	//construct all factories
	_bfactory = new ABulletFactory();
	_cfactory = new ACannonFactory();
	_lfactory = new ALevelFactory();
	_sfactory = new AShipFactory();
	_tfactory = new ATileFactory();

	//initialize models
	_player = 0;
	_enemies = std::map<unsigned int, Ship*>();
	_bullets = std::map<unsigned int, Bullet*>();

	//parse_levels();
	parse_levels();
	if(_lfactory->get_objects().empty()){
		Level* l = (Level*) _lfactory->create_object();
		l->default_level();
	}
	// always "needed"
	Tile* t = (Tile*)_tfactory->create_object();
	t->default_tile();
}
Game::~Game(){
	_bfactory->stop();
	_cfactory->stop();
	_lfactory->stop();
	_sfactory->stop();
	_tfactory->stop();

	delete _bfactory;
	delete _cfactory;
	delete _lfactory;
	delete _sfactory;
	delete _tfactory;
}

// interface for setting up the game
void Game::start(){
	//starts up the game
	_clock_game_start.restart();
	_clock_last_update.restart();
	_clock_calc.restart();
	// levels should be read here
	notifyObservers();


	while(!_ended){
		loop(); // implement main loop of the game
	}
}
void Game::stop(){
	// should release all ships bullets and cannons
	_bfactory->stop();
	_cfactory->stop();
	_sfactory->stop();
	_lfactory->stop();
	_tfactory->stop();

	_ended = true;
}

// interface for visualization

double Game::get_fps(){
	return 1/_last_update_time;

}
unsigned int Game::get_score(){
	return _score;
}

double Game::get_center(){
	return _field_center;
}
Ship* Game::get_player(){
	return _player;
}
std::map<unsigned int, Ship*> Game::get_enemies(){
	_enemies = _sfactory->get_objects();
	_enemies.erase(_player->get_id());
	return _enemies;
}
std::map<unsigned int, Bullet*> Game::get_bullets(){
	_bullets = _bfactory->get_objects();
	return _bullets;
}
std::map<unsigned int, Tile*> Game::get_tiles(){
	_tiles = _tfactory->get_objects();
	return _tiles;
}
std::vector<Tile*> Game::get_ctiles(){
	return _current_tiles;
}
std::map<unsigned int, Level*> Game::get_levels(){
	_levels = _lfactory->get_objects();
	return _levels;
}
Level* Game::get_clevel(){
	return _current_level;
}
std::set<std::pair<double, double >> Game::get_explosions(){
	return _explosions;
}

// interface for input
GameState Game::get_game_state(){
	return _gamestate;
}
void Game::enter_main(){
	_clock_game_start.restart();
	if(_gamestate != STARTUP){
		printf(ANSI_COLOR_RED "Game cannot be entered when not in STARTUP mode"
				ANSI_COLOR_RESET "\n");
		return;
	}else{
		_gamestate = MAIN;
		return;
	}
}
void Game::start_game(unsigned int id){
	// MAIN -> GAME
	if(_clock_game_start.getElapsedTime().asSeconds() <= 1) return;
	if(_gamestate != MAIN){
		printf(ANSI_COLOR_RED"Game cannot be started when not in MAIN mode"
				ANSI_COLOR_RESET "\n");
		return;
	}else{
		_gamestate = GAME;
		_current_level = _levels.at(id); // load level
		start_level();
		return;
	}
}
void Game::quit_game(){
	//GAME/PAUSED -> MAIN
	if(_gamestate != GAME &&  _gamestate != PAUSED){
		printf(ANSI_COLOR_RED "Game cannot be quit when not in GAME or PAUSED mode"
				ANSI_COLOR_RESET "\n");
		return;
	}else{
		_gamestate = MAIN;
		// end game
		stop_level();
		return;
	}
}
void Game::pause_game(){
	// GAME -> PAUSED
	if(_gamestate!= GAME){
		printf(ANSI_COLOR_RED "Game cannot be paused when not in GAME mode"
				ANSI_COLOR_RESET "\n");
		return;
	}else{
		_gamestate = PAUSED;
		return;
	}
}
void Game::resume_game(){
	// PAUSED -> GAME
	if(_gamestate != PAUSED){
		printf(ANSI_COLOR_RED "game cannot be resumed when not in PAUSED mode"
				ANSI_COLOR_RESET "\n");
		return;
	}else{
		_gamestate = GAME;
		return;
	}
}
void Game::exit_dead_won(){
	//  DEAD/WON -> MAIN
	if(_gamestate != DEAD && _gamestate != WON){
		printf(ANSI_COLOR_RED "game can't end dead/won screen when not in DEAD mode"
				ANSI_COLOR_RESET"\n");
	}else{
		_gamestate = MAIN;
	}
}

void Game::player_move(Movement mov){
	switch(mov){
	case UP:{
		double pos = _player->get_location().second;
		pos += _last_update_time*_current_level->get_player_speed();
		if(pos >= _field_center+10) pos = _field_center+10;
		_player->set_location(_player->get_location().first, pos);
	}break;
	case DOWN:{
		double pos = _player->get_location().second;
		pos -= _last_update_time*_current_level->get_player_speed();
		if(pos <= _field_center-9.9) pos = _field_center-9.9;
		_player->set_location(_player->get_location().first, pos);
	}break;
	case LEFT:{
		double pos = _player->get_location().first;
		pos -= _last_update_time*_current_level->get_player_speed();
		if(pos <= -10) pos = -10;
		_player->set_location(pos, _player->get_location().second);
	}break;
	case RIGHT:{
		double pos = _player->get_location().first;
		pos += _last_update_time*_current_level->get_player_speed();
		if(pos >= 10) pos = 10;
		_player->set_location(pos, _player->get_location().second);
	}
	}
}
void Game::player_shoot(){
	if(_clock_last_shot.getElapsedTime() > sf::seconds(0.5)){
		_clock_last_shot.restart(); // reset clock
		Bullet* b = (Bullet*)_bfactory->create_object();
		b->set_location(_player->get_location().first,
				_player->get_location().second);
		b->set_direction(0,1);
		b->set_type(BPLAYER);
	}
}

void Game::loop(){
	// main loop of game, will be called upon by start
	// pre game update commands
	_last_update_time = _clock_last_update.restart().asSeconds();
	notifyObservers();
	//game update
	switch(_gamestate){
	case STARTUP:{
		// nothing to do here for now
	}break;
	case MAIN:{
		// nothing to do here, maybe add level selection
		// level selection is handled by the view,
		// it will tell us what to do
	}break;
	case GAME:{
		// update game modules
		/* check field center against winning of game */
		/* move all objects */
		//set center of field

		_field_center += _current_level->get_speed()*_last_update_time;

		if (_field_center >= _current_level->get_length()+50){
			stop_level();
			_gamestate = WON;
			break;
		}
		update_player_position();
		update_enemy_position();
		update_bullet_position();
		update_enemies_shoot();

		//load new tiles/enemies
		update_tiles();
		/* resolve collisions */
		update_collisions();

		update_enemy_death();
		update_player_death();



	}break;
	case PAUSED:{
		// nothing to do here, wait for input

	}break;
	case DEAD:{
		// nothing to do here, all should be writen when game stops

	}break;
	case WON:{
		// nothing to do here, idem as DEAD
	}
	}
	//post game update commands
}
void Game::player_dies(){
	// player dies before end of level
	_gamestate = DEAD;
	stop_level();
}
void Game::player_wins(){
	// player wins the level, return to main menu
	//ends the game,
	//stores the score into highscores
	_gamestate = WON;
	stop_level();
}
void Game::stop_level(){
	//empty all lists and factories
	_bfactory->stop();
	_cfactory->stop();
	_sfactory->stop();

	_current_level=0;
	_current_tiles= std::vector<Tile*>();

	_player = 0;
	_enemies = std::map<unsigned int, Ship*>();
	_bullets = std::map<unsigned int, Bullet*>();

	_field_center = 0;

	//TODO add score write

}

void Game::start_level(){
	if(_current_level == 0){
		printf("level not set quiting \n");
		return;
	}
	// initialize players
	_player = (Ship*)_sfactory->create_object();
	_player->set_type(PLAYER);
	_player->set_location(0,-9);
	_player->untake_damage(10);

	// initialize 3 beginning tiles
	_tiles = _tfactory->get_objects();
	Tile *t1, *t2, *t3;
	std::string tn1 = _current_level->get_tile(0);
	std::string tn2 = _current_level->get_tile(1);
	std::string tn3 = _current_level->get_tile(2);
	for(auto tile: _tiles){
		if (tile.second->get_type() == tn1) t1 = new Tile(*tile.second);
		if (tile.second->get_type() == tn2) t2 = new Tile(*tile.second);
		if (tile.second->get_type() == tn3) t3 = new Tile(*tile.second);
	}

	t1->set_location(0,0);
	t2->set_location(0,20);
	t3->set_location(0,30);

	spawn_enemies(t1);
	spawn_enemies(t2);
	spawn_enemies(t3);

	_current_tiles = std::vector<Tile*> {t1, t2, t3};

	_score =0;
	return;
}

void Game::update_collisions(){
	// run over all objects
	// player takes damage by enemy bullets and crashing into enemies
	// enemies take damage by colliding with player bullets
	_bullets = _bfactory->get_objects();
	_enemies = _sfactory->get_objects();
	_explosions.clear();

	// loop over all ships
	for(auto its = _enemies.begin(); its != _enemies.end(); its++){
	// case 1: check ships colliding
		if(its->second->get_type() == ENEMY){
			// a player cannot collide with itself
			if(get_e_distance(
					its->second->get_location(),
					_player->get_location())
					<= 0.4){
				_player->take_damage(1);
				its->second->take_damage(1);
				_explosions.insert(_player->get_location());
			}
		}
		// case 2: check ships colliding with bullets
		_bullets = _bfactory->get_objects();
		for(auto itb = _bullets.begin(); itb != _bullets.end(); itb++){
			// case 1 enemy bullet and player
			if(its->second->get_type() == PLAYER && itb->second->get_type() == BENEMY){
				if(get_e_distance(its->second->get_location(),
						itb->second->get_location()) <= 0.2){
					// ship takes damage
					its->second->take_damage();
					// bullet should be destroyed
					_bfactory->remove_object(itb->second);
				}
			}
			//case 2: player bullet and enemy
			else if(its->second->get_type() == ENEMY && itb->second->get_type() == BPLAYER){
				if(get_e_distance(its->second->get_location(),
					itb->second->get_location()) <= 0.3){
					// ship takes damage
					its->second->take_damage();
					// bullet should be destroyed
					_bfactory->remove_object(itb->second);
					_score++;
				}
			}
		}

	}


	_bullets = _bfactory->get_objects();
	_enemies = _sfactory->get_objects();
	_enemies.erase(_player->get_id());
}
void Game::update_enemy_death(){
	// check all enemies on damage = 0;
	//if damage =0 is detected remove from enemy list
	for (auto it = _enemies.begin(); it != _enemies.end(); it++){
		if(it->second->get_damage() == 0){
			_sfactory->remove_object(it->second);
		}
	}
	_enemies = _sfactory->get_objects();
	_enemies.erase(_player->get_id());
}
void Game::update_player_death(){
	// check player on damage = 0
	// if player dies, function player dies should be called upon
	if(_player->get_damage() == 0){
		player_dies();
	}
}

void Game::update_player_position(){
	// standard scrolling, forces player up
	std::pair<double, double> pos = _player->get_location();

	double new_loc = pos.second+_current_level->get_speed()*_last_update_time;

	if (new_loc <= _field_center-9.9) new_loc = _field_center-9.9;
	if (new_loc >= _field_center+9.9) new_loc = _field_center+9.9;

	_player->set_location(pos.first,new_loc);
}
void Game::update_enemy_position(){
	// this is only needed when enemies have moving patterns,
	// which is not implemented at the moment

	// we will however check if an enemy is outside of the field
	_enemies = _sfactory->get_objects(); // update list
	_enemies.erase(_player->get_id()); // remove player

	for(auto it = _enemies.begin(); it != _enemies.end(); it++){
		// enemies can only exit the screen through falling of the bottom
		// so that's the only case we need to check
		if(it->second->get_location().second <= _field_center -10){
			// enemy has left the screen
			// player cannot be deleted from here
			if(it->second->get_type() == ENEMY)
			_sfactory->remove_object(it->second);
		}
	}
	_enemies = _sfactory->get_objects(); // update list
	_enemies.erase(_player->get_id()); // remove player
}
void Game::update_bullet_position(){
	// updates all bullets
	_bullets = _bfactory->get_objects();
	std::pair<double, double> pos;
	std::pair<double, double> dir;
	for (auto it = _bullets.begin(); it != _bullets.end(); it++){
		pos = it->second->get_location();
		dir = it->second->get_direction();
		double new_x = pos.first +
				dir.first*_current_level->get_bullet_speed()*_last_update_time;
		double new_y = pos.second +
				dir.second*_current_level->get_bullet_speed()*_last_update_time;
		// check new position on being outside field
		if(new_x <= -10 ||
			new_x >= 10 ||
			new_y <= _field_center-10||
			new_y >= _field_center+10){
			// remove bullet from lists
			_bfactory->remove_object(it->second);
		}else{
			it->second->set_location(new_x, new_y);

		}
	}
	_bullets = _bfactory->get_objects();
}

void Game::update_tiles(){
	// at each time we should be holding 3 tiles, tiles will be used
	// for enemy spawning (when loaded, and background imaging)
	if(_next_tile_index >= _current_level->get_tilecount())return;
	Tile* t1 = _current_tiles.at(0); // oldest tile
	if(t1->get_location().second <= _field_center -15){
		Tile* t2 = _current_tiles.at(1);
		Tile* t3 = _current_tiles.at(2); // newest tile
		Tile* t4;

		// oldest tile is despawning, load new tile
		std::string tilename =_current_level->get_tile(_next_tile_index);
		for(auto it:_tiles){
			if(it.second->get_type() == tilename){
				t4 = new Tile(*it.second); // we need to copy the tile
				break;
			}
		}
		t4->set_location(t3->get_location().first, t3->get_location().second+10);


		_next_tile_index++;
		_current_tiles.at(0) = t2;
		_current_tiles.at(1) = t3;
		_current_tiles.at(2) = t4;
		spawn_enemies(t4);
	}
}

void Game::update_enemies_shoot(){
	_enemies = _sfactory->get_objects();
	_enemies.erase(_player->get_id());

	for(auto en: _enemies){
		if(ran4()>0.999){
			Bullet* b = (Bullet*)_bfactory->create_object();
			b->set_location(en.second->get_location().first,
					en.second->get_location().second);
			double x = _player->get_location().first - en.second->get_location().first;
			double y = _player->get_location().second - en.second->get_location().second;
			b->set_direction(x,y);
			b->set_type(BENEMY);
		}
	}
}

void Game::spawn_enemies(Tile* t){
	std::pair<double, double> center = t->get_location();

	std::vector<std::pair<double, double>> pos = t->get_spawn();
	for(auto position : pos){
		Ship* ne = (Ship*)_sfactory->create_object();
		ne->set_location(position.first, position.second+center.second);
		ne->set_type(ENEMY);
		ne->untake_damage(1);
	}
	_enemies = _sfactory->get_objects(); // update list
	_enemies.erase(_player->get_id()); // remove player
}

void Game::parse_levels(){
	// this function will take care of all level parsing stuff

	//open file containing all tile - filenames
	// tile must be constructed first since we will link them in the level
	try{
		std::ifstream tilefiles;
		tilefiles.open("../resources/tiles.txt");
		if(!tilefiles.is_open()) throw std::runtime_error("Could not open file");

		// for each filename in this file parse the pointed at file in a tile class
		std::string line;
		while(std::getline(tilefiles, line)){
			try{
				Tile* t = (Tile*)_tfactory->create_object();
				t->parse_file("../resources/tiles/"+line);
			}catch(std::runtime_error& e){
				std::cout<<e.what()<<std::endl;
			}
		}
	}
	catch(std::runtime_error& e){
		std::cout<<e.what()<<std::endl;
		printf("couldn't parse all tiles, verify tiles.txt\n");
	}

	_tiles = _tfactory->get_objects();

	//open file containing all level - filenames
	// will be using tiles from the parsed tiles,
	// if a tile does not exist, it will return to a default tile;
	try{
		std::ifstream levelfiles;
		levelfiles.open("../resources/levels.txt");
		if(!levelfiles.is_open()) throw std::runtime_error("Could not open file");

		// for each filename in this file parse the pointed at file in a tile class
		std::string line;
		while(std::getline(levelfiles, line)){
			try{
				Level* l = (Level*)_lfactory->create_object();
				l->parse_file("../resources/levels/"+line);
			}catch(std::runtime_error& e){
				std::cout<<e.what()<<std::endl;
			}
		}
	}
	catch(std::runtime_error& e){
		std::cout<<e.what()<<std::endl;
		printf("couldn't parse all tiles, verify tiles.txt\n");
	}
	catch(...){
		printf("unreasonable error");
	}

}



}


