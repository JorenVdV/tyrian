/**
 * ObservableGame.cpp
 *
 *  Created on: Dec 23, 2013
 *      Author: jweiren
 */

#include "../util/Observer.h"
#include <algorithm>

namespace ty {
/**
 * @brief Constructor for class ObservableGame
 */
ObservableGame::ObservableGame() {
}
/**
 * @brief Destructor for class ObservableGame
 */
ObservableGame::~ObservableGame() {
}
/**
 * @brief Notifies all observers of the game that the state has been altered
 */
void ObservableGame::notifyObservers(){
	for(unsigned int i=0; i<m_observers.size(); i++){
		m_observers.at(i)->update();
	}
}
/**
 * @brief Attaches a new observer to the game
 * @param observer
 * 				New observer to be added
 */
void ObservableGame::attach(util::Observer* observer){
	if(std::find(m_observers.begin(),
			m_observers.end(),
			observer)
	== m_observers.end()) m_observers.push_back(observer);
}
/**
 * @brief Detaches an observer from the game
 * @param observer
 * 				Observer to be detached
 */
void ObservableGame::detach(util::Observer* observer){
	std::vector<util::Observer*> observers;
	for(unsigned int i=0; i<m_observers.size(); i++){
		if(m_observers.at(i) != observer){
			observers.push_back(m_observers.at(i));
		}
	}
	m_observers = observers;
}



} /* namespace engine */
