/**
 * @file ObservableGame.h
 * @class engine::ObservableGame
 *
 * @brief Base class for an Observable game
 *
 * Contains attached observers whom can be detached and notified.
 *
 *	@author Joren Van de Vondel
 *  Created on: Dec 23, 2013
 */

#ifndef OBSERVABLEGAME_H_
#define OBSERVABLEGAME_H_

#include <vector>

namespace util{
	class Observer;
}

namespace ty{
class ObservableGame {
public:
	ObservableGame();
	virtual ~ObservableGame();

	void notifyObservers();
	void attach(util::Observer* observer);
	void detach(util::Observer* observer);

private:
	std::vector<util::Observer*> m_observers;
};
}


#endif /* OBSERVABLEGAME_H_ */
