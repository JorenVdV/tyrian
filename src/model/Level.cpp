/*
 * Level.cpp
 *
 *  Created on: 02 Jan 2015
 *      Author: jweiren
 */

#include "Level.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

namespace ty {

Level::Level():
	_name(""),
	_speed(0),
	_player_speed(0),
	_bullet_speed(0){
	_tiles = std::vector<std::string>();

}

Level::~Level() {
}

void Level::parse_file(std::string file){
	try{
		using boost::property_tree::ptree;
		ptree pt;

		// tiles will be defined in xml
		read_xml(file, pt);
		_name = pt.get<std::string>("level.name", "default");
		_speed = pt.get<double>("level.speed",0.1);
		_player_speed = pt.get<double>("level.playerspeed", 0.1);
		_bullet_speed = pt.get<double>("level.bulletspeed", 0.1);
		BOOST_FOREACH(ptree::value_type &v,
				pt.get_child("level.tiles")){
			_tiles.push_back(v.second.data());
		}
	}catch(...){
		printf("couldn't parse file: %s\n",file.c_str());
		printf("tile shall be reset to default settings\n");
		default_level();
	}
	_length = _tiles.size()*10;
}
void Level::default_level(){
	_name = "default";
	_speed = 0.1;
	_player_speed = 0.1;
	_bullet_speed = 0.2;
	for(int i=0; i<=100; i++){
		_tiles.push_back("default");
	}

	_length = _tiles.size()*10;
}

} /* namespace ty */
