/*
 * Tile.h
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#ifndef TILE_H_
#define TILE_H_

#include "Object.h"
#include <string>
#include <vector>

namespace ty {


class Tile: public Object {
	// a tile is a parseable object that contains a background image/color
	// it also must contain all the enemies and where they will spawn on the tile
	std::string _type; // this will be read from the config file
	std::string _background;
	std::vector< std::pair<double, double> > _espawn;
	// can be a vector sinds list will not be changed
public:
	//! constructor
	Tile();
	//! copy constructor
	Tile(Tile& t);
	//! destructor
	virtual ~Tile();

	//! getter for the enemy spawn list
	std::vector< std::pair<double, double> > get_spawn(){return _espawn;};
	//! getter for the tile type
	std::string get_type(){return _type;};
	//! getter for the background file
	std::string get_background(){ return _background;};
	//! tile parser
	/*!
	 * same functionality as the level parser
	 */
	void parse_file(std::string file);
	//! construct a default tile
	void default_tile();
};

} /* namespace ty */

#endif /* TILE_H_ */
