/*
 * ABulletFactory.h
 *
 *  Created on: 27 Dec 2014
 *      Author: jweiren
 */

#ifndef ABULLETFACTORY_H_
#define ABULLETFACTORY_H_

#include "AbstractFactory.h"
#include "Bullet.h"

namespace ty {
//! Bullet factory
/*!
 * Responsible for the creation and destruction of bullet type objects
 */
class ABulletFactory: public AbstractFactory {
private:
	std::map<unsigned int, Bullet*> _objects;
public:
	//! constructor
	/*!
	 * Does not need to do many special things, since its only
	 * task is to contain the objects
	 */
	ABulletFactory();

	//! destructor
	/*!
	 * Verifies that the container is empty, they should have
	 * been emptied before destruction
	 */
	~ABulletFactory();

	//! get all objects contained by this factory
	/*!
	 * returns a copy of the internal list of objects
	 */
	std::map<unsigned int, Bullet*> get_objects(){
		return _objects;
	}
	//! create a new Bullet, upcast is needed by caller.
	/*!
	 * Create a new bullet and then returns it to the caller
	 * by an Object pointer, objects should be recast the Bullet
	 * by the caller
	 */
	Object* create_object();

	//! removes an object from the list
	/*!
	 * Only way to remove an object permanently is to call
	 * upon this function (or stop to remove all)
	 */
	void remove_object(Object* obj);

	//! removes all objects hold in the container
	/*!
	 * ends a level or game, removes all objects int he container.
	 */
	void stop(void);

};

} /* namespace ty */

#endif /* ABULLETFACTORY_H_ */
