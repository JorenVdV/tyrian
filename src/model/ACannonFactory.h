/*
 * ACannonFactory.h
 *
 *  Created on: 27 Dec 2014
 *      Author: jweiren
 */

#ifndef ACANNONFACTORY_H_
#define ACANNONFACTORY_H_

#include "AbstractFactory.h"
#include "Cannon.h"
#include <unordered_set>

namespace ty {
//! Cannon Factory
/*!
 * Responsible for the creation and destruction
 * of cannon type objects
 */

class ACannonFactory: public AbstractFactory {
private:
	std::map<unsigned int, Cannon*> _objects;
public:
	//! constructor
	/*!
	 * Does not need to do many special things, since its only
	 * task is to contain the objects
	 */
	ACannonFactory();

	/*!
	 * Verifies that the container is empty, they should have
	 * been emptied before destruction
	 */
	virtual ~ACannonFactory();

	//! get all objects contained by this factory
	/*!
	 * returns a copy of the internal list of objects
	 */
	std::map<unsigned int, Cannon*> get_objects(){
		return _objects;
	}


	//! create a new Cannon, upcast is needed by caller.
	/*!
	 * Create a new bullet and then returns it to the caller
	 * by an Object pointer, objects should be recast the Cannon
	 * by the caller
	 */
	Object* create_object();

	//! removes an object from the list
	/*!
	 * Only way to remove an object permanently is to call
	 * upon this function (or stop to remove all)
	 */
	void remove_object(Object* obj);

	//! removes all objects hold in the container
	/*!
	 * ends a level or game, removes all objects int he container.
	 */
	void stop(void);
};

} /* namespace ty */

#endif /* ACANNONFACTORY_H_ */
