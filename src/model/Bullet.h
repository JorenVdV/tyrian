/*
 * @file Bullet.h
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#ifndef BULLET_H_
#define BULLET_H_

#include "Object.h"

namespace ty {
//! enum BType
/*!
 * defines the types of bullets there are in this game.
 * derived from object
 */
enum BType{BPLAYER, BENEMY};

//! class Bullet
/*!
 * Bullets are the objects shot by the ships
 */
class Bullet: public Object {
private:
	// a bullet has a direction, the combined vector should always
	// have a length equal to 1
	std::pair<double, double> _direction;
	BType _type;
public:
	//! basic constructor
	Bullet();
	//! special constructor getting an direction
	Bullet(double x, double y);
	//! destructor
	virtual ~Bullet();

	//! setter for the direction member
	void set_direction(double x, double y);
	//! getter for the direction member
	std::pair<double, double> get_direction(void);

	//! getter for the type member
	BType get_type(void);
	//! setter for the type member
	void set_type(BType b){_type =b;};

};

} /* namespace ty */

#endif /* BULLET_H_ */
