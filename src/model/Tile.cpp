/*
 * Tile.cpp
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#include "Tile.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

namespace ty {

Tile::Tile() {
	// TODO Auto-generated constructor stub

}

Tile::~Tile() {
	// TODO Auto-generated destructor stub
}

Tile::Tile(Tile& t):
	Object(t.get_location().first, t.get_location().second){
	// copy constructor
	_type = t.get_type();
	_background  =t.get_background();
	_espawn =t.get_spawn();
}

void Tile::parse_file(std::string file){
	// test if file exist
	try{
		using boost::property_tree::ptree;
		ptree pt;

		// tiles will be defined in xml
		read_xml(file, pt);
		_type = pt.get<std::string>("tile.tilename", "default");
		_background = pt.get<std::string>("tile.tilebground", "default");
		BOOST_FOREACH(ptree::value_type &v,
				pt.get_child("tile.enemies")){
			_espawn.push_back(std::make_pair(v.second.get<double>("x"), v.second.get<double>("y")));
		}
	}catch(...){
		printf("couldn't parse file: %s",file.c_str());
		printf("tile shall be reset to default settings");
		default_tile();
	}

}

void Tile::default_tile(){
	_type = "default";
	_background = "default.png";
	_espawn.push_back(std::make_pair(0,0));
}



} /* namespace ty */
