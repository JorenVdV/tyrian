/*
 * Canon.h
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#ifndef CANON_H_
#define CANON_H_

#include "Object.h"

namespace ty {
//! Class Cannon
/*!
 * cannons used by enemies and players, are at the moment not implemented in the game
 * the cannon class exist and can be added as a feature but at the moment
 * the player shoots upwards and enemies shoot directed to the player.
 * Cannons can be used as an extension for the player so that he can shoot in multiple directions
 */
class Cannon: public Object {
private:
	std::pair<double, double> _direction;
public:
	//! constructor
	Cannon();
	//! another constructor
	Cannon(double x, double y, double dx, double xy);
	//! destructor
	virtual ~Cannon();
	//! setter for direction
	void set_direction(double x, double y);
	//! getter for direction
	std::pair<double, double> get_direction(void);
};

} /* namespace ty */

#endif /* CANON_H_ */
