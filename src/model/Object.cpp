/*
 * Object.cpp
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#include "Object.h"
#include "../util/id_generator.h"

namespace ty {

Object::Object():
	_location(std::make_pair(0,0))
{
	_id = id_generator();
}

Object::Object(double x, double y):
	_location(std::make_pair(x,y))
{
	_id = id_generator();
}

Object::~Object()
{
}

void Object::set_location(double x, double y)
{
	_location = std::make_pair(x, y);
}

std::pair<double, double> Object::get_location(void)
{
	return _location;
}

unsigned int Object::get_id(void)
{
	return _id;
}

bool Object::equals(Object* b)
{
	return (_id == b->get_id());
}


} /* namespace ty */
