/*
 * Object.h
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include <utility>

namespace ty {
//! base class for all object in the game
/*!
 * class serves as base class for all the other objects in the model
 * the base class has a location and a unique id (unless more objects are
 * produced than there are unsigned ints)
 */
class Object {
private:
	// id: unique way to identify the object
	unsigned int _id;
	// location: all objects should be findable in the grid
	std::pair<double, double> _location;
public:
	//!constructor
	Object();
	//! constructor with location information
	Object(double x, double y);
	//! destructor
	virtual ~Object();
	//! setter for location
	void set_location(double x, double y);
	//! getter for locaction
	std::pair<double, double> get_location(void);
	//! getter for id
	unsigned int get_id(void);
	//! comparison check
	/*!
	 * compares two object by their id
	 */
	bool equals(Object* b);

};

} /* namespace ty */

#endif /* OBJECT_H_ */
