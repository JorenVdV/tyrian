/*
 * Cannon.cpp
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#include "Cannon.h"
#include <cmath>

namespace ty {

Cannon::Cannon():
	_direction(std::make_pair(1,0))
{
}

Cannon::Cannon(double x, double y, double dx, double dy):
		Object(x, y)
{
	// rescale x and y so that the combined vector has a length 1
	double new_x = sqrt(pow(dx,2)/(pow(dx,2)+pow(dy,2)));
	double new_y = sqrt(pow(dy,2)/(pow(dx,2)+pow(dy,2)));

	_direction = std::make_pair(new_x, new_y);
}

Cannon::~Cannon()
{
}

void Cannon::set_direction(double x, double y)
{
	// rescale x and y so that the combined vector has a length 1
	double new_x = sqrt(pow(x,2)/(pow(x,2)+pow(y,2)));
	double new_y = sqrt(pow(y,2)/(pow(x,2)+pow(y,2)));

	_direction = std::make_pair(new_x, new_y);
}

std::pair<double, double> Cannon::get_direction(void)
{
	return _direction;
}


} /* namespace ty */
