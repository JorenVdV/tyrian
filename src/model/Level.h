/*
 * Level.h
 *
 *  Created on: 02 Jan 2015
 *      Author: jweiren
 */

#ifndef LEVEL_H_
#define LEVEL_H_

#include "Object.h"
#include "Tile.h"

namespace ty {
//! class Level
/*!
 * contains all the information to start a level.
 * At the moment a highscore system is not implemented
 * but if done, it should be done in this class
 */
class Level: public Object {
private:
	// name
	std::string _name;
	// list of tiles
	std::vector<std::string> _tiles;
	// speed
	double _speed;
	double _player_speed;
	double _bullet_speed;

	int _length;
	// length
	// ?
public:
	//! constructor
	Level();
	//!destructor
	virtual ~Level();
	//! getter for name
	std::string get_name(){return _name;};
	//!getter for level speed
	double get_speed(){return _speed;};
	//! getter for player speed
	double get_player_speed(){return _player_speed;};
	//! getter for bullet speed
	double get_bullet_speed(){return _bullet_speed;};
	//! getter for tile at position i
	/*!
	 * boundaries check should be done at the caller
	 */
	std::string get_tile(int i){return _tiles.at(i);};
	// ! getter for the size of the tiles member
	unsigned int get_tilecount(){return _tiles.size();};
	//! parser for the level
	/*!
	 * parses a level given a certain file,
	 * if the file is not compatible an error is thrown
	 * \param the file
	 */
	void parse_file(std::string file);
	//! loads a default level
	void default_level();
	//! gets the overal length of the game
	int get_length(){return _length;};
};

} /* namespace ty */

#endif /* LEVEL_H_ */
