/*
 * AShipFactory.cpp
 *
 *  Created on: 27 Dec 2014
 *      Author: jweiren
 */

#include "AShipFactory.h"

namespace ty {

AShipFactory::AShipFactory() {
	// initialize list
	_objects = std::map<unsigned int, Ship*>();
}

AShipFactory::~AShipFactory() {
	// needs to make sure that all objects are released
	if(!_objects.empty()){
		printf("In ABulletFactory::~ABulletFactory(): \n objects list was not empty before deletion\n");
		stop();
	}
}

Object* AShipFactory::create_object(){
	// create new bullet
	Ship* s = new Ship();
	// add bullet to the collection
	_objects.insert(std::make_pair(s->get_id(),s));
	// return pointer to the bullet
	return s;
}

void AShipFactory::remove_object(Object* obj){
	//check if we hold obj id
	if(_objects.find(obj->get_id()) == _objects.end()){
		printf("A loose object was found id:");
		printf("%u \n", obj->get_id());
	}
	else{
		_objects.erase(obj->get_id()); // remove object from list
		delete obj; // delete the object
	}
}

void AShipFactory::stop(void){
	// this resets the factory for another run of the game,
	// could also be called upon by the destructor;
	// but when all goes well this shouldn't happen
	try{
	for(auto it = _objects.begin(); it != _objects.end(); it++){
		// loop over all items
		Object* obj = it->second;
		delete obj;
	}
	_objects.clear(); // remove all dangling pointers
	}catch(...){
		// we should not come here
		printf("Something went wrong, ABulletFactory::stop()\n");
	}
}

} /* namespace ty */
