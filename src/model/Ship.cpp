/*
 * Ship.cpp
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#include "Ship.h"
#include <cmath>
#include <stdio.h>
#include <limits>

namespace ty {

Ship::Ship():
	_damage(0),
	_type(ENEMY)
{
	_cannons = std::map<unsigned int, Cannon*>(); //init empty list
}

Ship::Ship(double x, double y, ShipType type, int damage):
	Object(x,y),
	_damage(damage),
	_type(type)
{
	_cannons = std::map<unsigned int, Cannon*>(); //init empty list
}

Ship::~Ship()
{
	// should release the canons at the canonfactory
	// could be done before
}

void Ship::take_damage(unsigned int damage /*=1*/)
{
	if (damage >= _damage) _damage = 0;
	else _damage -= damage;
}

void Ship::untake_damage(unsigned int damage /*=1*/)
{
	if(_damage >= std::numeric_limits<unsigned int>::max()-damage)
		_damage = std::numeric_limits<unsigned int>::max();
	else _damage += damage;
}

bool Ship::is_alive(void)
{
	return (_damage>0);
}

unsigned int Ship::get_damage(void)
{
	return _damage;
}

void Ship::add_cannon(Cannon* cannon)
{
	// check that canon does not yet exist inside the set
	for (auto it = _cannons.begin(); it != _cannons.end(); ++it)
	{
		if(it->second->equals(cannon))return;
	}
	_cannons.insert(std::make_pair(cannon->get_id(),cannon));
}

void Ship::remove_cannon(Cannon* cannon)
{
	for (auto it = _cannons.begin(); it != _cannons.end(); ++it)
	{
		if(it->second->equals(cannon)){
			_cannons.erase(it);
			return;
		}
	}
}

std::map<unsigned int, Cannon*> Ship::get_cannons(void)
{
	return _cannons;
}



} /* namespace ty */
