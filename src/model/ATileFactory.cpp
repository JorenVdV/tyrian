/*
 * ATileFactory.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: jweiren
 */

#include "ATileFactory.h"

namespace ty{

ATileFactory::ATileFactory() {
	_objects = std::map<unsigned int, Tile*>();
}

ATileFactory::~ATileFactory() {
	if(!_objects.empty()){
		printf("In ABulletFactory::~ABulletFactory(): \n objects list was not empty before deletion\n");
		stop();
	}
}

Object* ATileFactory::create_object(){
	Tile* t = new Tile();
	_objects.insert(std::make_pair(t->get_id(), t));
	return t;
}

void ATileFactory::remove_object(Object* obj){
	//check if we hold obj id
	if(_objects.find(obj->get_id()) == _objects.end()){
		printf("A loose object was found id:");
		printf("%u \n", obj->get_id());
	}
	else{
		_objects.erase(obj->get_id()); // remove object from list
		delete obj; // delete the object
	}
}

void ATileFactory::stop(void){
	try{
	for(auto it = _objects.begin(); it != _objects.end(); it++){
		// loop over all items
		Object* obj = it->second;
		delete obj;
	}
	_objects.clear(); // remove all dangling pointers
	}catch(...){
		// we should not come here
		perror("Something went wrong, ABulletFactory::stop()\n");
	}
}

}

