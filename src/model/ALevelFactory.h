/*
 * ALevelFactory.h
 *
 *  Created on: Jan 5, 2015
 *      Author: jweiren
 */

#ifndef SRC_MODEL_ALEVELFACTORY_H_
#define SRC_MODEL_ALEVELFACTORY_H_

#include "AbstractFactory.h"
#include "Level.h"

namespace ty {
//! Level factory
/*!
 * Responsible for the creation and destruction
 * of level type objects
 */
class ALevelFactory: public ty::AbstractFactory {
private:
	std::map<unsigned int,Level*> _objects;
public:

	//! constructor
	/*!
	 * Does not need to do many special things, since its only
	 * task is to contain the objects
	 */
	ALevelFactory();

	/*!
	 * Verifies that the container is empty, they should have
	 * been emptied before destruction
	 */
	virtual ~ALevelFactory();

	//! get all objects contained by this factory
	/*!
	 * returns a copy of the internal list of objects
	 */
	std::map<unsigned int, Level*> get_objects(){
		return _objects;
	}

	//! create a new Level, upcast is needed by caller.
	/*!
	 * Create a new bullet and then returns it to the caller
	 * by an Object pointer, objects should be recast the Level
	 * by the caller
	 */
	Object* create_object();

	//! removes an object from the list
	/*!
	 * Only way to remove an object permanently is to call
	 * upon this function (or stop to remove all)
	 */
	void remove_object(Object* obj);

	//! removes all objects hold in the container
	/*!
	 * ends a level or game, removes all objects int he container.
	 */
	void stop(void);
};
}

#endif /* SRC_MODEL_ALEVELFACTORY_H_ */
