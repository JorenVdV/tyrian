/*
 * ALevelFactory.cpp
 *
 *  Created on: Jan 5, 2015
 *      Author: jweiren
 */

#include "ALevelFactory.h"

namespace ty {

ALevelFactory::ALevelFactory() {
	//init list
	_objects = std::map<unsigned int, Level*>();
}

ALevelFactory::~ALevelFactory() {
	if(!_objects.empty()){
		printf("In ALevelFactory::~ALevelFactory(): \n objects list was not empty before deletion\n");
		stop();
	}
}

Object* ALevelFactory::create_object(){
	//create new Level
	Level* l = new Level();
	_objects.insert(std::make_pair(l->get_id(), l));
	return l;
}

void ALevelFactory::remove_object(Object* obj){
	//check if we hold obj id
	if(_objects.find(obj->get_id()) == _objects.end()){
		printf("A loose object was found id:");
		printf("%u \n", obj->get_id());
	}
	else{
		_objects.erase(obj->get_id()); // remove object from list
		delete obj; // delete the object
	}
}

void ALevelFactory::stop(void){
	try{
		for(auto it = _objects.begin(); it != _objects.end(); it++){
			// loop over all items
			Object* obj = it->second;
			delete obj;
		}
		_objects.clear(); // remove all dangling pointers
	}catch(...){
			// we should not come here
			printf("Something went wrong, ABulletFactory::stop()\n");
	}
}

}


