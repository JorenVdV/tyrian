/*
 * Ship.h
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#ifndef SHIP_H_
#define SHIP_H_

#include "Object.h"
#include "Cannon.h"
#include <map>

namespace ty {
//! enum containing all types of Ships
enum ShipType{PLAYER, ENEMY};
//! class Ship
/*!
 * Class defining all shiptype objects, player and enemies
 * primary function on top of being an object is to record
 * the damage taken.
 */
class Ship: public Object {
private:
	unsigned int _damage;
	std::map<unsigned int, Cannon*> _cannons;
	ShipType _type;
public:
	//! constructor
	Ship();
	//! another constructor
	Ship(double x, double y, ShipType type, int damage);
	//! destructor
	virtual ~Ship();
	//! decreases damage data member
	void take_damage(unsigned int damage=1);
	//! increases damage data member
	void untake_damage(unsigned int damage=1);
	//! returns true if damage != 0
	bool is_alive(void);
	//! returns the value of damage
	unsigned int get_damage(void);
	//! adds a cannon to the ship
	void add_cannon(Cannon* cannon);
	//! removes a cannon from the ship
	void remove_cannon(Cannon* cannon);
	//! returns a list of cannons
	std::map<unsigned int, Cannon*> get_cannons(void);
	//! getter for the ship type
	ShipType get_type(){return _type;};
	//! setter for the ship type
	void set_type(ShipType t){_type =t;};
};

} /* namespace ty */

#endif /* SHIP_H_ */
