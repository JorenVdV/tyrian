/*
 * @file AbstractFactory.h
 * @class ty::AbstractFactory
 *
 * @brief base class for all abstract factories
 *
 * @author Joren Van de Vondel
 *
 *  Created on: 19 Dec 2014
 */

#ifndef ABSTRACTFACTORY_H_
#define ABSTRACTFACTORY_H_

#include "Object.h"
#include <unordered_set>
#include <map>

namespace ty {

//! Abstract factory base class
/*!
  defines the template for all the other factories
  maybe in a later state the factory could be made in a template class
  and the inheritance could be discarded
 */
class AbstractFactory {
public:
	virtual ~AbstractFactory(){};
	/**
	 * @brief virtual create method of the object
	 * @return an object type
	 */
	virtual Object* create_object() =0;
	virtual void remove_object(Object*) =0;
	/**
	 * @brief virtual stop of the factory, called when factory is no longer required
	 * stopping the factory makes sure all the entities are removed safely
	 * @return void
	 */
	virtual void stop()=0;
};

} /* namespace ty */

#endif /* ABSTRACTFACTORY_H_ */
