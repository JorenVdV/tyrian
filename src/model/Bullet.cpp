/*
 * Bullet.cpp
 *
 *  Created on: 24 Dec 2014
 *      Author: jweiren
 */

#include "Bullet.h"
#include <cmath>

namespace ty {

Bullet::Bullet():
		_direction(std::make_pair(1, 0))
{
}

Bullet::Bullet(double x, double y)
{
	// rescale x and y so that the combined vector has a length 1
	double new_x = sqrt(pow(x,2)/(pow(x,2)+pow(y,2)));
	double new_y = sqrt(pow(y,2)/(pow(x,2)+pow(y,2)));

	_direction = std::make_pair(new_x, new_y);
}

Bullet::~Bullet()
{
}

void Bullet::set_direction(double x, double y)
{
	// rescale x and y so that the combined vector has a length 1
	double new_x = x>=0?
			sqrt(pow(x,2)/(pow(x,2)+pow(y,2))) :
			-sqrt(pow(x,2)/(pow(x,2)+pow(y,2)));
	double new_y = y >=0?
			sqrt(pow(y,2)/(pow(x,2)+pow(y,2))):
			-sqrt(pow(y,2)/(pow(x,2)+pow(y,2)));

	_direction = std::make_pair(new_x, new_y);
}

std::pair<double, double> Bullet::get_direction(void)
{
	return _direction;
}

BType Bullet::get_type(void){
	return _type;
}

} /* namespace ty */
