/*
 * AShipFactory.h
 *
 *  Created on: 27 Dec 2014
 *      Author: jweiren
 */

#ifndef ASHIPFACTORY_H_
#define ASHIPFACTORY_H_

#include "AbstractFactory.h"
#include "Ship.h"
#include <unordered_set>

namespace ty {
//! Ship factory
/*!
 * Responsible for all the ship objects creation and
 * destruction.
 */
class AShipFactory: public AbstractFactory {
private:
	std::map<unsigned int,Ship*> _objects;
public:
	//! constructor
	/*!
	 * Does not need to do many special things, since its only
	 * task is to contain the objects
	 */
	AShipFactory();

	/*!
	 * Verifies that the container is empty, they should have
	 * been emptied before destruction
	 */
	virtual ~AShipFactory();

	//! get all objects contained by this factory
	/*!
	 * returns a copy of the internal list of objects
	 */
	std::map<unsigned int, Ship*> get_objects(){
		return _objects;
	}

	//! create a new Ship, upcast is needed by caller.
	/*!
	 * Create a new bullet and then returns it to the caller
	 * by an Object pointer, objects should be recast the Ship
	 * by the caller
	 */
	Object* create_object();

	//! removes an object from the list
	/*!
	 * Only way to remove an object permanently is to call
	 * upon this function (or stop to remove all)
	 */
	void remove_object(Object* obj);

	//! removes all objects hold in the container
	/*!
	 * ends a level or game, removes all objects int he container.
	 */
	void stop(void);
};

} /* namespace ty */

#endif /* ASHIPFACTORY_H_ */
