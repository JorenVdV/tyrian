/*
 * @file GameView.h
 * @class view::GameView
 * @Author Joren Van de Vondel
 *
 *  Created on: Jan 7, 2015
 */

#ifndef SRC_VIEW_GAMEVIEW_H_
#define SRC_VIEW_GAMEVIEW_H_

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include "../controller/Game.h"
#include "../util/Observer.h"

namespace view {

//! GameView class
/*!
 Main class for displaying the game, will inherit from Observer
 and RenderWindow.

 This class will be linked to the game and display all needed
 graphical interface elements
 */
class GameView: public sf::RenderWindow, public util::Observer {
public:

	//! Constructor
	/*!
	  Sets up the GameView element, and links it to the game
	  \param game, which it observes
	 */
	GameView(ty::Game* game, bool hd=true);

	//! Destructor
	/*!
	  Takes care of nothing, all should be freed by now,
	  or doe not count to the responsibillities of this
	  class
	 */
	virtual ~GameView();

	//! update the view
	/*!
	  function that updates the view and listens for input
	 */
	void update();

	//! shows the screen
	/*!
	  called upon from the constructor, but in earlier versions
	  needed to be called upon from the main thread. Needed to
	  relocated its calling to constructor since the constructor
	  needed the dimensions of the window.
	 */
	void show();


private:
	// startup functions

	//! creates a windows
	/*!
	 dimensions of the window are set here, they are however
	 still static since no settings-file/class was implemented.
	 */
	void create_window();

	//! loads all backgrounds
	/*!
	 This function is responsible for the loading of all the
	 gamestate backgrounds.
	 If a file cannot be openend an error is thrown.
	 */
	void load_backgrounds();

	//! loads all tile_backgrounds
	/*!
	 This function is responsible for the loading of all the
	 tile backgrounds.
	 If a file cannot be openend an error is thrown.
	 */
	void load_tile_backgrounds();

	//! loads all ship sprites
	/*!
	 This function is responsible for the loading of all the
	 ship sprites.
	 If a file cannot be openend an error is thrown.
	 */
	void load_ship_sprites();

	//! loads all bullet sprites
	/*!
	 This function is responsible for the loading of all the
	 bullet sprites.
	 If a file cannot be openend an error is thrown.
	 */
	void load_bullet_sprites();

	//! loads startup music
	/*!
	 This function is responsible for the loading the startup
	 music.
	 If a file cannot be openend an error is thrown.
	 */
	void load_startup_music();

	//! loads menu music
	/*!
	 This function is responsible for the loading the menu
	 music.
	 If a file cannot be openend an error is thrown.
	 */
	void load_menu_music();

	//! loads game music
	/*!
	 This function is responsible for the loading the game
	 music.
	 If a file cannot be openend an error is thrown.
	 */
	void load_game_musiC();

	//! loads dead music
	/*!
	 This function is responsible for the loading the dead
	 music.
	 If a file cannot be openend an error is thrown.
	 */
	void load_dead_music();

	//! loads won music
	/*!
	 This function is responsible for the loading the won
	 music.
	 If a file cannot be openend an error is thrown.
	 */
	void load_won_music();

	//! loads explosion sounds
	/*!
	 This function is responsible for the loading the
	 explosion sound.
	 If a file cannot be openend an error is thrown.
	 */
	void load_explosion_music();

	//! loads shoot sounds
	/*!
	 This function is responsible for the loading the
	 shoot sound.
	 If a file cannot be openend an error is thrown.
	 */
	void load_shoot_music();

	//! loads the font
	/*!
	 A font is necessary for the game to functions, since the
	 default font was removed form the sfml -library.
	 */
	void load_font();


	// ingame functions

	//! draws the startup screen
	/*!
	 draws the startup screen using the gameview elements
	 can only be called upon when the game is in STARTUP
	 mode.
	 */
	void draw_startup();

	//! draws the main selection menu
	/*!
	  draws the main selection menu using the levels from the
	  game, the gameview is responsible for holding and updating
	  the current index of level that is displayed. And making sure
	  that this index corresponds to an existing level.
	 */
	void draw_main();

	//! draws the game when played
	/*!
	  Draws the main game screen,
	  ships bullets and backgrounds
	  BUG: does not display the tile background thanks to a misconnect of the
	  texture, can not find source of this bug though. Texture should remain
	  in memory while in the list, but somehow they do not link up to their
	  tile name.
	 */
	void draw_game();

	//! draws the pause screen
	/*!
	 Displays the pause screen.
	*/
	void draw_paused();

	//! draws the dead screen
	/*!
	 Displays the game over screen.
	 */
	void draw_dead();

	//! draws the won screen
	/*!
	 * Displays the level won screen, also adds the score
	 */
	void draw_won();

	//! listens to the window events and links them to the game
	/*!
	 * Has cases for all possible window input and parses them to
	 * a manner that the game can process.
	 */
	void listen_to_window_events();

	//! listens to the keyboard events and links them to the game
	/*!
	 * Listens to all the keyboard events and filters those that are
	 * relevant for the current game state, gotten from the game.
	 * Translation to game processable data is also done here.
	 */
	void listen_to_keyboard_events();

	//! recalculates the coordinates to the right format for the gameview
	/*!
	 * Recalculates game coordinates to window coordinates.
	 */
	std::pair<double, double> recalc_coordinates(
			std::pair<double, double> p, std::pair<double, double> c);


	// items the view will hold in its memory
	ty::Game* _game;
	bool _hdgame;
	unsigned int _level_index;


	// static background images
	sf::Texture _startup_background;
	sf::Texture _menu_background;
	sf::Texture _paused_background;
	sf::Texture _dead_background;
	sf::Texture _won_background;

	sf::Sprite _background;
	sf::Sprite _enemy;
	sf::Sprite _player;
	sf::Sprite _pbullet;
	sf::Sprite _ebullet;
	// background images defined in tiles, if not found revert to
	// standard background image and link that one.
	std::map<std::string, sf::Texture> _tile_background;
	std::map<ty::ShipType, sf::Texture> _ship_sprite;
	std::map<ty::BType, sf::Texture> _bullet_sprite;

	// music used during the game
	sf::Music _startup_music;
	sf::Music _menu_music;
	sf::Music _game_music;
	sf::Music _dead_music;
	sf::Music _won_music;

	sf::SoundBuffer _explosion_buffer;
	sf::Sound _explosion_sound;
	sf::SoundBuffer _shoot_buffer;
	sf::Sound _shoot_sound;


	sf::Font _font;
	sf::Text _startup_text;
	sf::Text _main_text;
	sf::Text _dead_text;
	sf::Text _won_text;

	sf::Clock _last_instr;



};

} /* namespace view */

#endif /* SRC_VIEW_GAMEVIEW_H_ */
