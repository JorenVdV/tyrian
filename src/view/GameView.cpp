/*
 * GameView.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: jweiren
 */

#include "GameView.h"
#include "../util/colortext.h"
#include "../util/utilf.h"
#include <stdexcept>
#include <list>
#include <map>
#include <boost/lexical_cast.hpp>
#include <iostream>


namespace view {

GameView::GameView(ty::Game* game, bool hd):
		_game(game),
		_hdgame(hd)
{
	//setFramerateLimit(1);
	game->attach(this); // add view to observers
	show();
	// load all necessary files
	if(_hdgame){
		try{
			load_backgrounds();
			load_tile_backgrounds();
			load_ship_sprites();
			load_bullet_sprites();

			load_startup_music();
			load_menu_music();
			load_game_musiC();
			load_dead_music();
			load_won_music();
			load_explosion_music();
			load_shoot_music();

			_enemy.setTexture(_ship_sprite.at(ty::ENEMY));
			_enemy.setOrigin(_enemy.getGlobalBounds().width/2,
					_enemy.getGlobalBounds().height/2);
			_player.setTexture(_ship_sprite.at(ty::PLAYER));
			_player.setOrigin(_player.getGlobalBounds().width/2,
					_player.getGlobalBounds().height/2);
			_pbullet.setTexture(_bullet_sprite.at(ty::BPLAYER));
			_pbullet.setOrigin(_pbullet.getGlobalBounds().width/2,
					_pbullet.getGlobalBounds().height/2);
			_ebullet.setTexture(_bullet_sprite.at(ty::BENEMY));
			_ebullet.setOrigin(_ebullet.getGlobalBounds().width/2,
					_ebullet.getGlobalBounds().height/2);



		} catch (std::runtime_error& e){
			_hdgame =false;
			std::cout<<e.what()<<std::endl;
			printf("Game will continue in non-hd mode");
		}
	}
	try{
		load_font();
	}catch(std::runtime_error& e){
		printf("No font was found, cannot continue");
		throw;
	}

	//load all textures into sprites

	// load all sounds

	// set texts

	_startup_text = sf::Text("PRESS ENTER TO START", _font, 80);
	_main_text = sf::Text("Choose level",
			_font, 80);
	_dead_text = sf::Text("GAME OVER", _font, 80);
	_won_text = sf::Text("Level complete", _font, 80);
	_startup_text.setOrigin(_startup_text.getGlobalBounds().width/2,
			_startup_text.getGlobalBounds().height*0.8);
	_main_text.setOrigin(_main_text.getGlobalBounds().width/2,
			_main_text.getGlobalBounds().height/2);
	_dead_text.setOrigin(_dead_text.getGlobalBounds().width/2,
			_dead_text.getGlobalBounds().height/2);
	_won_text.setOrigin(_won_text.getGlobalBounds().width/2,
			_won_text.getGlobalBounds().height/2);

	_startup_text.setPosition(this->getSize().x/2, this->getSize().y/2);
	_startup_text.setColor(sf::Color::White);
	_main_text.setPosition(this->getSize().x/2, this->getSize().y*0.01);
	_dead_text.setPosition(this->getSize().x/2, this->getSize().y*0.5);
	_won_text.setPosition(this->getSize().x/2, this->getSize().y*0.5);


}

GameView::~GameView() {
}

void GameView::update(){
	// main loop of game, will be called upon when observers are being
	// notified by the controller
	if(_game == 0)return; //cannot update empty game

	clear(sf::Color::Black); //clear the screen
	// draw the new screen
	switch(_game->get_game_state()){
	case ty::STARTUP:{
		// draw startup screen
		draw_startup();
	}break;
	case ty::MAIN:{
		// draw main menu
		draw_main();
	}break;
	case ty::GAME:{
		// draw game board
		draw_game();
	}break;
	case ty::PAUSED:{
		// draw paused screen
		draw_paused();
	}break;
	case ty::DEAD:{
		//draw dead screen
		draw_dead();
	}break;
	case ty::WON:{
		// draw won screen
		draw_won();

	}break;
	}

	//update input
	listen_to_window_events();
	listen_to_keyboard_events();

}

void GameView::show(){
	create_window();
	display();
}

void GameView::create_window(){
	// static screen resolution for now
	create(sf::VideoMode(800, 600,32),"Tyrian");
	clear(sf::Color::Black);
}

void GameView::load_backgrounds(){
	// loads the backgrounds from the resources map
	if(!_startup_background.loadFromFile("../resources/backgrounds/startup_background.png"))
		throw std::runtime_error("could not load file /resources/backgrounds/startup_background.png");
	if(!_menu_background.loadFromFile("../resources/backgrounds/menu_background.png"))
		throw std::runtime_error("could not load file /resources/backgrounds/menu_background.png");
	if(!_paused_background.loadFromFile("../resources/backgrounds/paused_background.png"))
		throw std::runtime_error("could not load file /resources/backgrounds/paused_background.png");
	if(!_dead_background.loadFromFile("../resources/backgrounds/dead_background.png"))
		throw std::runtime_error("could not load file /resources/backgrounds/dead_background.png");
	if(!_won_background.loadFromFile("../resources/backgrounds/won_background.png"))
		throw std::runtime_error("could not load file /resources/backgrounds/won_background.png");
}
void GameView::load_tile_backgrounds(){
	//loads for each tile found in the game the background
	if(_game==0)throw std::runtime_error("no game was found");
	else{
		auto tiles = _game->get_tiles();
		for(auto it = tiles.begin(); it!= tiles.end(); it++){
			sf::Texture temp;
			std::string file = "../resources/tile_backgrounds/"+it->second->get_background();
			if(!temp.loadFromFile(file))
					throw std::runtime_error("Could not load file:"+file);
			else{
				_tile_background.insert(std::make_pair(it->second->get_type(), temp));
			}
		}
	}
}
void GameView::load_ship_sprites(){
	sf::Texture player, enemy;
	if(!player.loadFromFile("../resources/ship_sprites/player2.png"))
		throw std::runtime_error("could not load file /resources/ship_sprites/player.png");
	if(!enemy.loadFromFile("../resources/ship_sprites/enemy2.png"))
		throw std::runtime_error("could not load file /resources/ship_sprites/enemy.png");

	_ship_sprite.insert(std::make_pair(ty::PLAYER, player));
	_ship_sprite.insert(std::make_pair(ty::ENEMY, enemy));
}
void GameView::load_bullet_sprites(){
	sf::Texture bplayer, benemy;
	if(!bplayer.loadFromFile("../resources/bullet_sprites/bplayer.png"))
		throw std::runtime_error("could not load file /resources/bullet_sprites/bplayer.png");
	if(!benemy.loadFromFile("../resources/bullet_sprites/benemy.png"))
		throw std::runtime_error("could not load file /resources/bullet_sprites/benemy.png");
	_bullet_sprite.insert(std::make_pair(ty::BPLAYER, bplayer));
	_bullet_sprite.insert(std::make_pair(ty::BENEMY, benemy));

}
void GameView::load_startup_music(){
	if(!_startup_music.openFromFile("../resources/sounds/startup.wav"))
		throw std::runtime_error("could not load file /resources/sounds/startup.wav");
}
void GameView::load_menu_music(){
	if(!_menu_music.openFromFile("../resources/sounds/menu.wav"))
		throw std::runtime_error("could not load file /resources/sounds/menu.wav");
}
void GameView::load_game_musiC(){
	if(!_game_music.openFromFile("../resources/sounds/game.wav"))
		throw std::runtime_error("could not load file /resources/sounds/game.wav");
}
void GameView::load_dead_music(){
	if(!_dead_music.openFromFile("../resources/sounds/dead.wav"))
		throw std::runtime_error("could not load file /resources/sounds/dead.wav");
}
void GameView::load_won_music(){
	if(!_won_music.openFromFile("../resources/sounds/won.wav"))
		throw std::runtime_error("could not load file /resources/sounds/won.wav");
}
void GameView::load_explosion_music(){
	if(!_explosion_buffer.loadFromFile("../resources/sounds/explosion.wav"))
		throw std::runtime_error("could not load file /resources/sounds/explision.wav");
	_explosion_sound.setBuffer(_explosion_buffer);
	_explosion_sound.setLoop(false);
}
void GameView::load_shoot_music(){
	if(!_shoot_buffer.loadFromFile("../resources/sounds/shoot.wav"))
		throw std::runtime_error("could not load file /resources/sounds/shoot.wav");
	_shoot_sound.setBuffer(_shoot_buffer);
	_shoot_sound.setLoop(false);
}
void GameView::load_font(){
	if(!_font.loadFromFile("../resources/font/font2.ttf"))
		throw std::runtime_error("could not load font");
}
void GameView::draw_startup(){
	// draws startup screen
	if(_hdgame){
		// play sounds
		// no other songs can be in play at this moment
		if(_startup_music.getStatus()  != sf::Music::Playing)
			_startup_music.play();

		//draw the background
		_background.setTexture(_startup_background);
		_background.setTextureRect(sf::IntRect(0,0,this->getSize().x, this->getSize().y));
		draw(_background);
	}
	draw(_startup_text);
	display();
}
void GameView::draw_main(){
	//draws main menu
	if(_hdgame){
		// handle music
		if(_startup_music.getStatus()  == sf::Music::Playing)
			_startup_music.stop();
		if(_menu_music.getStatus() != sf::Music::Playing){
			_menu_music.play();
		}
		if(_game_music.getStatus() == sf::Music::Playing){
			_game_music.stop();
		}
		if(_dead_music.getStatus() == sf::Music::Playing){
			_dead_music.stop();
		}
		if(_won_music.getStatus() == sf::Music::Playing){
			_won_music.stop();
		}

		//draw background
		_background.setTexture(_menu_background);
		_background.setTextureRect(sf::IntRect(0,0,this->getSize().x, this->getSize().y));
		draw(_background);
	}
	// set all text in window
	// write header
	draw(_main_text);
	// write levels
	auto levels = _game->get_levels();
	auto it = levels.begin();
	sf::Text levelname;
	levelname.setFont(_font);
	levelname.setCharacterSize(50);


	// case nr levels == 1
	if(levels.size()==1){
		// draw only the level name at the middle of the screen
		levelname.setString(it->second->get_name());
		levelname.setColor(sf::Color::Red);
		levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
		levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
		draw(levelname);
	}
	// case nr levels == 2
	else if(levels.size() == 2){
		switch (_level_index) {
		case 0:{
			// draw first red, second one white
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::Red);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
			draw(levelname);
			it++;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2+60);
			draw(levelname);
		}break;
		case 1:{
			// draw first red, second one white
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2-60);
			draw(levelname);
			it++;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::Red);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
			draw(levelname);
		}break;
		}
	}
	// case nr levels >= 3
	else{
		std::advance(it, _level_index);
		//case it == begin
		if(it == levels.begin()){
			// we need to add the last element as first
			it = levels.end();
			it--;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2-60);
			draw(levelname);

			it = levels.begin();
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::Red);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
			draw(levelname);

			it++;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2+60);
			draw(levelname);

		}
		//case it == last element
		else if (it++ == levels.end()){
			// we need to add the first element of the list
			it = levels.begin();
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2-60);
			draw(levelname);

			it = levels.end();
			it--;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::Red);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
			draw(levelname);

			it--;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2+60);
			draw(levelname);
		}
		// default
		else{
			it--;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2-60);
			draw(levelname);

			it++;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::Red);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2);
			draw(levelname);

			it++;
			levelname.setString(it->second->get_name());
			levelname.setColor(sf::Color::White);
			levelname.setOrigin(levelname.getGlobalBounds().width/2,levelname.getGlobalBounds().height/2);
			levelname.setPosition(this->getSize().x/2, this->getSize().y/2-60);
			draw(levelname);
		}
	}
	display();
}

void GameView::draw_game(){
	// draws game view

	std::pair<double, double> loc;
	std::pair<double, double> center =
			std::make_pair(0,_game->get_center());
	// if hd draw with images
	if(_hdgame){
		// handle music
		if(_startup_music.getStatus()  == sf::Music::Playing)
			_startup_music.stop();
		if(_menu_music.getStatus() == sf::Music::Playing){
			_menu_music.stop();
		}
		if(_game_music.getStatus() != sf::Music::Playing){
			_game_music.play();
		}
		if(_dead_music.getStatus() == sf::Music::Playing){
			_dead_music.stop();
		}
		if(_won_music.getStatus() == sf::Music::Playing){
			_won_music.stop();
		}

		//draw background

		auto tiles = _game->get_ctiles();
		for(auto tile : tiles){
			// this background should be in range
			// load background into image
			if(_tile_background.find(tile->get_type()) == _tile_background.end()){

			}else{
			_background.setTexture(_tile_background.at(tile->get_type()));
			_background.setTextureRect(sf::IntRect(0,0,this->getSize().x, this->getSize().y));
			_background.setOrigin(_background.getGlobalBounds().width/2,_background.getGlobalBounds().height/2);
			loc = tile->get_location();
			loc = recalc_coordinates(loc, center);
			_background.setPosition(sf::Vector2f(loc.first, loc.second));
			draw(_background);
			}
		}

		// draw enemies

		auto enemies = _game->get_enemies();
		for (auto it : enemies){
			//sprite should already be loaded
			loc = recalc_coordinates(it.second->get_location(), center);
			_enemy.setPosition(sf::Vector2f(loc.first, loc.second));
			draw(_enemy);
		}

		// draw player
		loc = recalc_coordinates(_game->get_player()->get_location(), center);
		_player.setPosition(sf::Vector2f(loc.first, loc.second));
		draw(_player);

		// draw bullets
		auto bullets = _game->get_bullets();
		for (auto it : bullets){
			//sprite should already be loaded
			loc = recalc_coordinates(it.second->get_location(), center);
			switch (it.second->get_type()){
			case ty::BPLAYER:{
				_pbullet.setPosition(sf::Vector2f(loc.first, loc.second));
				draw(_pbullet);
			}break;
			case ty::BENEMY:{
				_ebullet.setPosition(sf::Vector2f(loc.first, loc.second));
				draw(_ebullet);
			}
			}
		}

	}
	// if non hd use sprites
	else{
		// draw enemies
	    sf::CircleShape pshape(10);
	    pshape.setOrigin(pshape.getGlobalBounds().width/2,
	    		pshape.getGlobalBounds().height /2);
	    pshape.setFillColor(sf::Color::Red);

		auto enemies = _game->get_enemies();
		for (auto it : enemies){
			//sprite should already be loaded
			loc = recalc_coordinates(it.second->get_location(), center);
			pshape.setPosition(sf::Vector2f(loc.first, loc.second));
			draw(pshape);
		}

		// draw player
		pshape.setFillColor(sf::Color::Yellow);

		loc = recalc_coordinates(_game->get_player()->get_location(), center);
		pshape.setPosition(sf::Vector2f(loc.first, loc.second));
		draw(pshape);

		// draw bullets
		sf::CircleShape bshape(3);
	    bshape.setOrigin(bshape.getGlobalBounds().width/2,
	    		bshape.getGlobalBounds().height /2);
		auto bullets = _game->get_bullets();
		for (auto it : bullets){
			//sprite should already be loaded
			loc = recalc_coordinates(it.second->get_location(), center);
			switch (it.second->get_type()){
			case ty::BPLAYER:{
				bshape.setFillColor(sf::Color::Yellow);
				bshape.setPosition(sf::Vector2f(loc.first, loc.second));
				draw(bshape);
			}break;
			case ty::BENEMY:{
				bshape.setFillColor(sf::Color::Red);
				bshape.setPosition(sf::Vector2f(loc.first, loc.second));
				draw(bshape);
			}
			}
		}

	}
	// write text
	// draw score
	auto score = _game->get_score();
	sf::Text score_t;
	std::string score_s = "SCORE:"+ boost::lexical_cast<std::string>(score);
	score_t.setString(score_s);
	score_t.setCharacterSize(40);
	score_t.setFont(_font);
	score_t.setColor(sf::Color::White);
	score_t.setOrigin(score_t.getGlobalBounds().width/2,
			score_t.getGlobalBounds().height/2);
	score_t.setPosition(this->getSize().x*0.2, 40);
	draw(score_t);

	// draw health
	auto health = _game->get_player()->get_damage();
	score_s = "HEALTH:"+boost::lexical_cast<std::string>(health);
	score_t.setString(score_s);
	score_t.setOrigin(score_t.getGlobalBounds().width/2,
			score_t.getGlobalBounds().height/2);
	score_t.setPosition(this->getSize().x*0.8, 40);
	draw(score_t);
	display();
}
void GameView::draw_paused(){
	if(_hdgame){
		_background.setTexture(_paused_background);
		_background.setTextureRect(sf::IntRect(0,0,this->getSize().x, this->getSize().y));
	}
	sf::Text instr;
	instr.setString("Game Paused");
	instr.setOrigin(instr.getGlobalBounds().width/2,
			instr.getGlobalBounds().height/2);
	instr.setCharacterSize(60);
	instr.setFont(_font);
	instr.setColor(sf::Color::White);
	instr.setPosition(this->getSize().x*0.5, this->getSize().y*0.4);
	draw(instr);

	instr.setString("Press P to resume");
	instr.setOrigin(instr.getGlobalBounds().width/2,
			instr.getGlobalBounds().height/2);
	instr.setPosition(this->getSize().x*0.5, this->getSize().y*0.4);
	draw(instr);
	display();

}
void GameView::draw_dead(){
	if(_hdgame){
		// handle music
		if(_startup_music.getStatus()  == sf::Music::Playing)
			_startup_music.stop();
		if(_menu_music.getStatus() == sf::Music::Playing){
			_menu_music.stop();
		}
		if(_game_music.getStatus() == sf::Music::Playing){
			_game_music.stop();
		}
		if(_dead_music.getStatus() != sf::Music::Playing){
			_dead_music.play();
		}
		if(_won_music.getStatus() == sf::Music::Playing){
			_won_music.stop();
		}
	}
	draw(_dead_text);
	display();
}
void GameView::draw_won(){
	if(_hdgame){
		// handle music
		if(_startup_music.getStatus()  == sf::Music::Playing)
			_startup_music.stop();
		if(_menu_music.getStatus() == sf::Music::Playing){
			_menu_music.stop();
		}
		if(_game_music.getStatus() == sf::Music::Playing){
			_game_music.stop();
		}
		if(_dead_music.getStatus() == sf::Music::Playing){
			_dead_music.stop();
		}
		if(_won_music.getStatus() == sf::Music::Playing){
			_won_music.play();
		}
	}
	draw(_won_text);
	sf::Text instr;
	unsigned int score = _game->get_score();
	std::string score_t = "SCORE: "+ boost::lexical_cast<std::string>(score);
	instr.setString(score_t);
	instr.setCharacterSize(60);
	instr.setFont(_font);
	instr.setColor(sf::Color::White);
	instr.setOrigin(instr.getGlobalBounds().width/2,
			instr.getGlobalBounds().height/2);
	instr.setPosition(this->getSize().x*0.5, this->getSize().y*0.5 + 50);
	draw(instr);
	display();
}

void GameView::listen_to_window_events(){
	sf::Event event;
	while(pollEvent(event)){
		if(event.type == sf::Event::Closed){
			// end the game and close the window
			_game->stop();
			close();
			break;
		}
		if(event.key.code == sf::Keyboard::Escape){
			_game->stop();
			close();
			break;
		}
	}
}


void GameView::listen_to_keyboard_events(){
	switch(_game->get_game_state()){
	case ty::STARTUP:{
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
			_game->enter_main();
		}
	}break;
	case ty::MAIN:{
		// we show only 3 levels (they go round)

		if(_last_instr.getElapsedTime().asSeconds() <= 0.5) break;

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
			// select next level as "chosen"
			// case index = levels+1, return to 0;
			if(_level_index == _game->get_levels().size()-1) _level_index=0;
			// case = normal
			else _level_index+=1;
			_last_instr.restart();
		}

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
			// inverse of above
			//case index = 0, go to levels.size
			if(_level_index==0) _level_index = _game->get_levels().size()-1;
			// case = normal
			else _level_index-=1;
			_last_instr.restart();
		}

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
			// start the chosen level
			auto levels = _game->get_levels();
			auto it = levels.begin();
			std::advance(it, _level_index);
			_game->start_game(it->first);
			_last_instr.restart();
		}

	}break;
	case ty::GAME:{
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)||
				sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
			_game->player_move(ty::UP);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)||
				sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
			_game->player_move(ty::DOWN);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)||
				sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
			_game->player_move(ty::RIGHT);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)||
				sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
			_game->player_move(ty::LEFT);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
			_game->player_shoot();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::P)){
			if(_last_instr.getElapsedTime().asSeconds() <=0.5) break;
			_game->pause_game();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)){
			_game->quit_game();
		}
	}break;
	case ty::PAUSED:{
		if(_last_instr.getElapsedTime().asSeconds() <=0.5) break;
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::P)){
			_game->resume_game();
			_last_instr.restart();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)){
			_game->quit_game();
		}
	}break;
	case ty::DEAD:{
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
			_game->exit_dead_won();
			_last_instr.restart();
		}
	}break;
	case ty::WON:{
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
			_game->exit_dead_won();
			_last_instr.restart();
		}
	}break;
	}
}

std::pair<double, double> GameView::recalc_coordinates(std::pair<double, double> in,
		std::pair<double, double> center){
	// recalculate point to positive xy;
	double x, y;
	// remap to -10 10 interval
	x = in.first - center.first;
	y = in.second - center.second;
	// shift to positive axis and rescale to window size
	return std::make_pair((x+10)*(this->getSize().x/20), (10-y)*this->getSize().y/20);
}


} /* namespace view */
