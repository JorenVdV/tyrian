var searchData=
[
  ['ran4',['ran4',['../utilf_8cpp.html#a0709da5b575f9302530660492ee3b077',1,'ran4():&#160;utilf.cpp'],['../utilf_8h.html#a0709da5b575f9302530660492ee3b077',1,'ran4():&#160;utilf.cpp']]],
  ['recalc_5fcoordinates',['recalc_coordinates',['../classview_1_1GameView.html#a1bdfa60f1fcdc9ee2c6dbc4dd9d1d7c3',1,'view::GameView']]],
  ['remove_5fcannon',['remove_cannon',['../classty_1_1Ship.html#a75c91e551e3c85999e87fe142c239c90',1,'ty::Ship']]],
  ['remove_5fobject',['remove_object',['../classty_1_1AbstractFactory.html#a000cc9be53cd6c5269ba300059ea43c1',1,'ty::AbstractFactory::remove_object()'],['../classty_1_1ABulletFactory.html#a64051e76d89e30ea05c9375900e392f3',1,'ty::ABulletFactory::remove_object()'],['../classty_1_1ACannonFactory.html#a78662383f93639642890b53f8e3c37d7',1,'ty::ACannonFactory::remove_object()'],['../classty_1_1ALevelFactory.html#af383172be8393ab0a4b6929e5ffdf074',1,'ty::ALevelFactory::remove_object()'],['../classty_1_1AShipFactory.html#a4ff5cf76891c1d374b8e2e9d406a3c32',1,'ty::AShipFactory::remove_object()'],['../classty_1_1ATileFactory.html#a84145e2ab8bd8bb9cf16b103d1334cbc',1,'ty::ATileFactory::remove_object()']]],
  ['resume_5fgame',['resume_game',['../classty_1_1Game.html#a5925b8e48e2da403d72b6497a50bcdd4',1,'ty::Game']]]
];
