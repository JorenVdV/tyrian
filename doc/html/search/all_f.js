var searchData=
[
  ['set_5fdirection',['set_direction',['../classty_1_1Bullet.html#ae801fe3ae1df3603220d71f93460f3d7',1,'ty::Bullet::set_direction()'],['../classty_1_1Cannon.html#a029419018497256d0a619314a27f1d80',1,'ty::Cannon::set_direction()']]],
  ['set_5flocation',['set_location',['../classty_1_1Object.html#abd75c24723251c133d7b28a5e9e1dc88',1,'ty::Object']]],
  ['set_5ftype',['set_type',['../classty_1_1Bullet.html#a434f1e9bdadba563823930e54676f4aa',1,'ty::Bullet::set_type()'],['../classty_1_1Ship.html#ab8b7b728d9c8bedbb590407799f73d0c',1,'ty::Ship::set_type()']]],
  ['ship',['Ship',['../classty_1_1Ship.html',1,'ty']]],
  ['ship',['Ship',['../classty_1_1Ship.html#ae9ceda8655c7c8a85f0a2caf8de3cca5',1,'ty::Ship::Ship()'],['../classty_1_1Ship.html#a2b8caea3e180251557fc4f8182b9db75',1,'ty::Ship::Ship(double x, double y, ShipType type, int damage)']]],
  ['ship_2ecpp',['Ship.cpp',['../Ship_8cpp.html',1,'']]],
  ['ship_2eh',['Ship.h',['../Ship_8h.html',1,'']]],
  ['shiptype',['ShipType',['../namespacety.html#a6c2d28f318814555e0b99cc48238a3f2',1,'ty']]],
  ['show',['show',['../classview_1_1GameView.html#a557173306ab23e51bcaca420f7e95c8d',1,'view::GameView']]],
  ['spawn_5fenemies',['spawn_enemies',['../classty_1_1Game.html#a8b0cb104923d6c4732741ab607fe6f7e',1,'ty::Game']]],
  ['start',['start',['../classty_1_1Game.html#a75c471e2f6039cee1847b74185075039',1,'ty::Game']]],
  ['start_5fgame',['start_game',['../classty_1_1Game.html#afaeb8d504cff257595b315d457959563',1,'ty::Game']]],
  ['start_5flevel',['start_level',['../classty_1_1Game.html#ae7b8b650c504fc99ea2b9321b37aa0af',1,'ty::Game']]],
  ['startup',['STARTUP',['../namespacety.html#aa015c1ce7a8d0b6574651513a48a997da3f277c3ad7ab5d10a043eaff47ef5c85',1,'ty']]],
  ['stop',['stop',['../classty_1_1Game.html#a02bc80c128394c9590f6c3f4f3136976',1,'ty::Game::stop()'],['../classty_1_1AbstractFactory.html#af378abf45bb5206020f1fe4221593eef',1,'ty::AbstractFactory::stop()'],['../classty_1_1ABulletFactory.html#a70150e88ec81a989ddf66c471c1d3830',1,'ty::ABulletFactory::stop()'],['../classty_1_1ACannonFactory.html#acd46082d7bde2ebeeea14ebc79bde54b',1,'ty::ACannonFactory::stop()'],['../classty_1_1ALevelFactory.html#a2f1b17a25eed2e1e9161d2105b2b0970',1,'ty::ALevelFactory::stop()'],['../classty_1_1AShipFactory.html#ab3a3abb799899d7fd333d250a30738e9',1,'ty::AShipFactory::stop()'],['../classty_1_1ATileFactory.html#a7bb2d6fde530accb1f0cf7ebb3e07f41',1,'ty::ATileFactory::stop()']]],
  ['stop_5flevel',['stop_level',['../classty_1_1Game.html#a3b7a40fc230453ea378d6c1e77b288dd',1,'ty::Game']]]
];
