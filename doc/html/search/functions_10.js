var searchData=
[
  ['untake_5fdamage',['untake_damage',['../classty_1_1Ship.html#a8323aa8c63a0d380a977b2d62204ff3e',1,'ty::Ship']]],
  ['update',['update',['../classutil_1_1Observer.html#a74a601fe35357bfbf450c7f9047caafb',1,'util::Observer::update()'],['../classview_1_1GameView.html#aed7395b38336142d8abf8ed413ee27ac',1,'view::GameView::update()']]],
  ['update_5fbullet_5fposition',['update_bullet_position',['../classty_1_1Game.html#adc3da93827ffed9aa6b1c42f2493ae88',1,'ty::Game']]],
  ['update_5fcollisions',['update_collisions',['../classty_1_1Game.html#acdd9f72151b72f99857a0bc705d985c8',1,'ty::Game']]],
  ['update_5fenemies_5fshoot',['update_enemies_shoot',['../classty_1_1Game.html#a31078200ead91a38b5854064f250f10e',1,'ty::Game']]],
  ['update_5fenemy_5fdeath',['update_enemy_death',['../classty_1_1Game.html#a53481e84229392a3b6e6b9f17acb71a9',1,'ty::Game']]],
  ['update_5fenemy_5fposition',['update_enemy_position',['../classty_1_1Game.html#a07e1027c1d674e3019996c1416be1c7e',1,'ty::Game']]],
  ['update_5fplayer_5fdeath',['update_player_death',['../classty_1_1Game.html#a686b89e4dd7f8102afb41dbd223978a0',1,'ty::Game']]],
  ['update_5fplayer_5fposition',['update_player_position',['../classty_1_1Game.html#ac0d3261840d11ff73d452c8a4aa42bb5',1,'ty::Game']]],
  ['update_5ftiles',['update_tiles',['../classty_1_1Game.html#ad50564b36f3b9bb06c1bad3b66634cf9',1,'ty::Game']]]
];
